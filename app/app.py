from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from config import Config
import logging

""" Initialize the Flask app, API, database, ORM, and serializer """
db = SQLAlchemy()
api = Api()
ma = Marshmallow()

""" Setup logger configuration """
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s:%(levelname)s:%(message)s')

""" Setup logging to email_parse which can be found on the host """
file_handler = logging.FileHandler('/var/log/email_parser/email_parser.log')
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

""" Setup logger for logging to the container logs """
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)


def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)

    from resources import resources, AllEmailLogs, SingleEmailLog
    app.register_blueprint(resources)

    db.init_app(app)

    logger.info('Initializing database...')
    with app.app_context():
        db.create_all()
    logger.info('Done initializing database...')

    """ Setup the routes for our endpoints """
    api.add_resource(AllEmailLogs, '/', '/email_logs/all', '/email_logs/all/', )
    api.add_resource(SingleEmailLog, '/email_logs/<email_log_id>', '/email_logs/<email_log_id>/')

    api.init_app(app)
    ma.init_app(app)

    return app
