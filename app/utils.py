from flask_restful import abort
from email_log_parser import parse_email_logs, parse_date_to_datetime
from app import db
from models import EmailLogs
from sqlalchemy import exc
from app import logger


def parse_emails():
    """ Parse the email logs found in the mount from docker-compose then store details in the database """

    """ For every email found """
    for parsed_email in parse_email_logs('/usr/src/dataset'):
        """ Convert the date to an datetime so that dates are uniform and aware """
        parsed_email['converted_date'] = parse_date_to_datetime(parsed_email).isoformat()

        """ If the email log isn't found in the db already """
        if EmailLogs.query.filter_by(name=parsed_email['email_log']).first() is None:

            try:
                email_log = EmailLogs(name=parsed_email['email_log'],
                                      email_to=parsed_email['email_to'],
                                      email_from=parsed_email['email_from'],
                                      email_date=parsed_email['converted_date'],
                                      email_subject=parsed_email['email_subject'],
                                      email_message_id=parsed_email['message_id'], )
                db.session.add(email_log)
            except KeyError as e:
                logger.warning(f"Unable to parse {parsed_email['email_log']} => unable to find {e}")

            """ Save the details from the parsed email in the database """
            db.session.commit()


def abort_if_email_log_doesnt_exist(email_log_id):
    """ If a user is trying to go to a specific email_log but it doesn't exist then return a 404 """
    try:
        email_log = EmailLogs.query.filter_by(id=email_log_id).first()

        if email_log is None:
            abort(404, message=f"Email log {email_log_id} doesn't exist")
        else:
            return email_log
    except exc.DataError as e:
        logger.exception(e)
        abort(404, message=f"Email log {email_log_id} doesn't exist")
