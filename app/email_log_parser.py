from collections import namedtuple
from dateutil import parser
from datetime import datetime
import json
import re
import os
from app import logger


with open('./timezone_abbr_to_utc_offset.json', 'r') as tz_abbr_to_utc:
    """ If we can't find the time zone look for it in this timezone abbreviation to UTC offset file"""
    utc_offsets = json.load(tz_abbr_to_utc)


def tokenize(input_file):
    """ Read a input file and return an iterator with the 'Tokens' found """

    """ Declare the 'Tokens' we want to search for when reading the input file """
    Tokens = namedtuple('Tokens', ['email_pattern',
                                   'to_pattern',
                                   'from_pattern',
                                   'date_pattern',
                                   'subject_pattern',
                                   'message_id_pattern'])

    """ Declare the patterns associated with each 'Token' """
    patterns = Tokens(
        email_pattern=r'(?:[A-Z0-9!#$%&\'*+\/=?^_`{|}~.-]{1,64}'  # local name
                      r'|"[A-Z(),:;<>@[\]\s.]{1,62}")'  # local name variant with double quotes
                      r'@'
                      r'[A-Z0-9.()][A-Z0-9.()-]{1,62}[A-Z0-9.()][A-Z0-9.()-])'
                      r'>?\s*$',  # domain name & email_to/email_from end group
        to_pattern=r'^\s*To:[A-Z0-9\s!#$%&\'*+\/=?^_`{|}~.\"-]*(:?\s+|<)(?P<email_to>',  # email_to group start
        from_pattern=r'^\s*From:[A-Z0-9\s!#$%&\'*+\/=?^_`{|}~.\"-]*(:?\s+|<)(?P<email_from>',  # email_from group start
        date_pattern=r'^\s*Date:\s+(?P<date_time>'  # date_time group
                     r'(?P<date>:?'  # date group
                     r'(?P<date_week_day_abbr>[A-Z]{3}),\s+'  # date_week_day_abbr group
                     r'(?P<date_day>[0-9]{1,2})\s+'  # date_day group
                     r'(?P<date_month_abbr>[A-Z]{3})\s+'  # date_month_abbr group
                     r'(?P<date_year>[0-9]{4})'  # date_year group
                     r'|'  # date variant
                     r'(?P<date_day_>[0-9]{1,2})\s+'  # date_day group
                     r'(?P<date_month_abbr_>[A-Z]{3})\s+'  # date_mont_abbr group
                     r'(?P<date_year_>[0-9]{4}))'  # date_year group
                     r'(?P<time>\s+'  # time group
                     r'(?P<time_hour>[0-9]{2}):'  # time_hour group
                     r'(?P<time_min>[0-9]{2}):'  # time_min group
                     r'(?P<time_sec>[0-9]{2})\s+'  # time_sec group 
                     r'(?P<time_utc_offset>[+-]*(?:[0-9]{4})*)\s*\(*(?:'  # time_utc_offset group
                     r'(?P<time_zone_abbr>[A-Z]{3}))*\)*\s*$))',  # time_zone_abbr group
        subject_pattern=r'^Subject:\s*'
                        r'(?P<email_subject>(?:[^:]+|[^\n]+)$)(?=\n[^:]+:)',  # email_subject group
        message_id_pattern=r'^Message-ID:\s*<?'
                           r'(?P<message_id>[A-Z0-9.@-]+)>\s*$',  # message_id group
    )

    """ Join each 'Token' using OR in order to scan the file one time """
    email_log_pattern = '|'.join((patterns.from_pattern + patterns.email_pattern,
                                  patterns.to_pattern + patterns.email_pattern,
                                  patterns.date_pattern,
                                  patterns.subject_pattern,
                                  patterns.message_id_pattern))

    """ Attach the ignore case and multi-line flags to the email_log_patter """
    email_log_pattern = re.compile(email_log_pattern, re.I | re.M)

    """ Return the iterator for each match """
    try:
        with open(input_file, 'r') as email_log:
            logger.info(f'************ Reading {input_file} ****************')
            return re.finditer(email_log_pattern, email_log.read())
    except UnicodeDecodeError as e:
        logger.exception(f'UnicodeDecodeError: {e}')


def parse_email_logs(directory):
    """ Iterate over every file found in the directory parameter """

    os.chdir(directory)

    """ Check mounted directory for nested files """
    for dirpath, dirnames, filenames in os.walk(os.getcwd()):
        """ For every file in the directory """
        for email_log in filenames:
            result = dict()
            """ Set the name of the email_log file so that we can determine where all of the matches originated from """
            email_log = os.path.join(dirpath, email_log)
            result['email_log'] = email_log
            try:
                """ Search the text for the tokens we want """
                for match in tokenize(email_log):
                    """Look through the returned dictionary to store matches we want"""
                    for group, value in match.groupdict().items():
                        if value:
                            result[group] = value
                yield result
            except TypeError as e:
                logger.exception(f'TypeError: {e}')


def parse_date_to_datetime(parsed_email):
    """ Convert date str to datetime for storing in the database"""
    converted_date = datetime.utcnow()
    try:
        converted_date = parser.parse(parsed_email['date_time'])
    except KeyError as e:
        logger.exception(f'KeyError: {e}')
    except ValueError as e:
        logger.exception(f'ValueError: {e}')

    try:
        """ 
            If no time zone is associated with the converted_date 
            try again with a time_zone_abbr from utc_offsets 
        """
        if converted_date.tzinfo is None or converted_date.tzinfo.utcoffset(converted_date) is None:
            """ Attach the UTC offset from utc_offsets """
            parsed_email['date_time'] = f'{parsed_email["date_time"]} {utc_offsets[parsed_email["time_zone_abbr"]]}'
            converted_date = parser.parse(parsed_email['date_time'])
    except AttributeError as e:
        logger.exception(f'AttributeError: {e}')
    except KeyError as e:
        logger.exception(f'KeyError: {e}')
    return converted_date
