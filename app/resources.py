from flask import Blueprint, jsonify
from flask_restful import Resource
from models import EmailLogs, EmailLogsSchema
from utils import parse_emails, abort_if_email_log_doesnt_exist


resources = Blueprint('resources', __name__)


class AllEmailLogs(Resource):
    """ Accept GET requests to see all of the parsed email details """
    def get(self):
        parse_emails()
        all_email_logs = EmailLogs.query.all()
        email_logs_schema = EmailLogsSchema(strict=True)
        result = email_logs_schema.dump(all_email_logs, many=True)
        """ Return all of the email logs details as JSON """
        return jsonify(result.data)


class SingleEmailLog(Resource):
    """ Accept GET requests to see a single parsed email's details """
    def get(self, email_log_id):
        parse_emails()
        email_log = abort_if_email_log_doesnt_exist(email_log_id)
        email_logs_schema = EmailLogsSchema(strict=True)
        result = email_logs_schema.dump(email_log)
        """ Return the specific email's details or a 404 not found message """
        return jsonify(result.data)
