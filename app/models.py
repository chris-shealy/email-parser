from app import db, ma


class EmailLogs(db.Model):
    """ Model for EmailLogs to be stored in the database """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256), unique=True, nullable=False)
    """ local name = 64 char domain = 63 char """
    email_to = db.Column(db.String(127), unique=False, nullable=True)
    email_from = db.Column(db.String(127), unique=False, nullable=True)
    email_date = db.Column(db.DateTime(timezone=True), nullable=True)
    email_subject = db.Column(db.String(256), unique=False, nullable=True)
    email_message_id = db.Column(db.String(256), unique=False, nullable=True)

    def __repr__(self):
        return f'<EmailLogs ' \
               f'{self.name} ' \
               f'{self.email_to} ' \
               f'{self.email_from} ' \
               f'{self.email_date}' \
               f'{self.email_subject} ' \
               f'{self.email_message_id}>'


class EmailLogsSchema(ma.ModelSchema):
    """ EmailLogs schema for serialization """
    class Meta:
        model = EmailLogs
