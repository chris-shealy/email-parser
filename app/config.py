import os


class Config:
    """
        Get the PostgreSQL connection string from the docker-compose file
        so that the connection string isn't in the repo
    """
    POSTGRES_CONFIG = {
        'user': os.environ['POSTGRES_USER'],
        'password': os.environ['POSTGRES_PASSWORD'],
        'host': os.environ['POSTGRES_HOST'],
        'port': os.environ['POSTGRES_PORT'],
        'db': os.environ['POSTGRES_DB'],
    }

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = 'postgresql://{user}:{password}@{host}:{port}/{db}'.format(**POSTGRES_CONFIG)

    WERKZEUG_DEBUG_PIN = 'off'
