#!/bin/bash

while ! pg_isready -h "${POSTGRES_HOST}" -p "${POSTGRES_PORT}" > /dev/null 2> /dev/null; do
   echo "Connecting to PostgreSQL Failed"
   sleep 1
 done

>&2 echo "PostgreSQL is up - executing command"

exec python run.py