# Email Parser #

*[Email Parser at DockerHub](https://cloud.docker.com/repository/docker/chshealy/email-parser/general)*

Email parser is an application which utilizes container orchestration using Docker Compose.

The application provides the following:

* Parses files from the dataset mount `./dataset`
* Saves the parsed data to a PostgreSQL database using a persistant data mount `./postgres_data`
* Presents the parsed data as JSON with the following API endpoints:
    * View all parsed email logs
        * `http://0.0.0.0:5000/`
        * `http://0.0.0.0:5000/email_logs/all`
        * `http://0.0.0.0:5000/email_logs/all/`
    * View a single parsed email log
        * `http://0.0.0.0:5000/email_logs/<email_log_id>`
        * `http://0.0.0.0:5000/email_logs/<email_log_id>/`
        

## Getting Started ##

Clone the repo and perform the following to get started:
```
# Change directories into the repo
cd email-parser/

# Start docker compose on the yml
docker-compose up
```

Add files to the newly created `./dataset` directory and view your parsed data at the API endpoints mentioned previously

```
[
  {
    "email_date": "2011-04-01T14:17:41+00:00", 
    "email_from": "infos@contact-darty.com", 
    "email_message_id": "20110401161739.E3786358A9D7B977@contact-darty.com", 
    "email_subject": "Cuit Vapeur 29.90 euros, Nintendo 3DS 239 euros, GPS TOM TOM 139 euros... decouvrez VITE tous les bons plans du weekend !", 
    "email_to": "1000mercis@cp.assurance.returnpath.net", 
    "id": 1, 
    "name": "20110401_1000mercis_14461469_html.msg"
  }, 
  {
    "email_date": "2011-04-01T04:19:52+00:00", 
    "email_from": "survey@mindspaymails.com", 
    "email_message_id": "MP1301631592801EH10491@mindspay.com", 
    "email_subject": "Paid Mail : Offer #10491 get $4.00", 
    "email_to": "aamarketinginc@cp.monitor1.returnpath.net", 
    "id": 2, 
    "name": "20110401_aamarketinginc_14456749_html.msg"
  }, 
  ...
  ...
  ...
  ]
```

## Additional Info ##

Depending on how you start docker-compose you may need to change permissions on the mounted folders.

This can easily be done with the following command (Linux):

```
chown my_user:my_user dataset/
```